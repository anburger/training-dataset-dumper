export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
asetup Athena,main,latest

# add h5ls
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
source ${SCRIPT_PATH%/*}/add-h5-tools.sh
source ${SCRIPT_PATH%/*}/allow-breaking-edm.sh
