"""
Lightweight setup for retagging with systematic variations
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FTagDumper.trackUtil import applyTrackSys
from FTagDumper.jetUtil import applyJetSys

from AthenaConfiguration.ComponentFactory import CompFactory

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConfig import (
    InDetTrackTruthOriginToolCfg,
)
from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg,
)
from FlavorTagDiscriminants.FoldDecoratorConfig import FoldDecoratorCfg

import copy, re


FLIPMAP = {
    'STANDARD': '',
    'SIMPLE_FLIP': 'Simple',
    'FLIP_SIGN': 'Flip',
    'NEGATIVE_IP_ONLY': 'Neg'
}


def retagging(
        flags,
        jet_collection,
        track_collection="InDetTrackParticles",
        jet_systs=[],
        jet_sigma=1.0,
        track_systs=[],
        decorate_track_truth=False,
        flip_configs=['STANDARD']):

    acc = ComponentAccumulator()

    output_jets = _renamed(
        jet_collection,
        jet_systs=jet_systs)

    if jet_systs:
        acc.merge(
            applyJetSys(
                jet_systs,
                jet_collection,
                jet_sigma,
                output_jet_collection=output_jets
            )
        )
    else:
        output_jets = jet_collection

    pvCol = "PrimaryVertices"
    simpleTrackIpPrefix = 'simpleIp_'

    if track_systs:
        varied_tracks = '_'.join([track_collection] + track_systs)
        acc.merge(applyTrackSys(
            track_systs,
            track_collection,
            jet_collection,
            output_tracks=varied_tracks
        ))
        track_collection = varied_tracks

    # The decorations need to be added after the systematics are
    # applied, in case there's some smearing which changes the d0
    # info.
    #
    # NOTE: this is NOT the same impact parameter we use in most
    # flavor tagging. It is a poor man's equivelent, which can run in
    # AthAnalysis and saves us a few minutes of setup time.
    #
    acc.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            f'PoorMansIpAugmenterAlg_{output_jets}',
            trackContainer=track_collection,
            primaryVertexContainer=pvCol,
            prefix=simpleTrackIpPrefix,
        )
    )

    # add some truth labels
    if decorate_track_truth:
        trackTruthOriginTool = acc.popToolsAndMerge(
            InDetTrackTruthOriginToolCfg(flags)
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
                'TruthParticleDecoratorAlg',
                trackTruthOriginTool=trackTruthOriginTool
            )
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
                f'TrackTruthDecoratorAlg_{track_collection}',
                trackContainer=track_collection,
                trackTruthOriginTool=trackTruthOriginTool
            )
        )


    # now we assicoate the tracks to the jet
    tracks_on_jet = '_'.join(["TracksForMinimalJetTag"] + track_systs)
    acc.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=output_jets,
        InputParticleCollection=track_collection,
        OutputParticleDecoration=tracks_on_jet,
    ))

    # Add jet fold decorator.
    #
    # This wasn't stored in the derivation I'm writing this for, but
    # it should be stored in the future. This block will liekly be
    # removable in the near future.
    acc.merge(FoldDecoratorCfg(flags, jetCollection=output_jets))

    # Now we have to add an algorithm that tags the jets The input and
    # output remapping is handled via a map in the multifold tool.
    #
    variableRemapping = {
        'BTagTrackToJetAssociator': tracks_on_jet,
        'btagIp_': simpleTrackIpPrefix,
    }
    # add multifold GNN
    nn_base = 'BTagging/20231205/GN2v01/antikt4empflow'
    pf_nns = [f'{nn_base}/network_fold{n}.onnx' for n in range(4)]
    for fc in flip_configs:
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg(
                name=f'myGN2_{fc}_{output_jets}',
                container=output_jets,
                constituentContainer=track_collection,
                decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                    name=f'myGN2_tool{fc}',
                    foldHashName='jetFoldHash',
                    nnFiles=pf_nns,
                    variableRemapping=variableRemapping,
                    flipTagConfig=fc,
                    trackLinkType='IPARTICLE'
                )
            )
        )

    return acc


def mungedConfig(
        config_dict,
        track_systs,
        jet_systs,
        jet_sigma,
        flip_configs
):
    """
    Return a modified version of the dumpster config, altered to support whatever systematics we're applying.
    """
    cd = config_dict
    nc = copy.deepcopy(cd)

    # rename the jet collection
    nc['jet_collection'] = _renamed(
        cd['jet_collection'],
        jet_systs=jet_systs)

    # rename the outputs
    flip_re = re.compile('(GN[1-9]v[0-9]{2})_(p.*)')
    if floats := cd['variables'].get('jet',{}).get('floats',[]):
        for f in list(floats):
            if m := flip_re.match(f):
                floats.remove(f)
                for fc in flip_configs:
                    newval = '{}{}_{}'.format(
                        m.group(1),
                        FLIPMAP[fc],
                        m.group(2)
                    )
                    floats.append(newval)
        nc['variables']['jet']['floats'] = floats

    return nc


def _renamed(collection, **systs):
    fullnames = [collection]
    for group in systs.values():
        for s in group:
            fullnames.append(s)
    return '_'.join(fullnames)
